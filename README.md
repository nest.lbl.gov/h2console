[h2console](https://gitlab.com/nest.lbl.gov/h2console) project provides as simple Web Application to expose the H2 database management interface in a Wildfly server.

Once the created WAR file has been deployed the console can be found at: [http://localhost:8080/h2console/h2](http://localhost:8080/h2console/h2)
